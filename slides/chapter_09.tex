\documentclass[size=screen,xcolor=table]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,multirow,mathtools}
\begin{document}
\title{Chapter 09: Sampling Distributions}
\author{Instructor: John Labarga}
\date{March 24, 2022}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Introduction}

\subsection{Functions of Random Variables}

\begin{frame}
  Since observations of random variables drawn from a distribution are
  random, functions calculated from them are also random.

  \vspace{3mm}

  If we draw a random sample from a distribution, and calculate a
  sample statistic like the mean or standard deviation, then that
  sample statistic is \emph{itself} a random variable.

  \vspace{3mm}

  To understand how well our sample statistics estimate their
  corresponding population parameters, we need to be able to quantify
  their sampling distributions.

  \vspace{3mm}

  \underline{Example}: suppose you sample from a
  $\mbox{Binomial}\left(1, \frac12 \right)$ 100 times. What is the
    distribution of $\bar{x}$?

\end{frame}

\begin{frame}
  \frametitle{Where does this fit in our roadmap?}
  
  Our final goal in this course is to do inference: extrapolate back
  from sample statistics to population parameters, and estimate them.

  \vspace{3mm}

  Two different samples from the same population will produce two
  different sets of numbers (as long as sampling from the population
  is random). This will also cause any statistics calculated from
  those samples to be different.

  \vspace{3mm}

  If we can determine the distribution of the sampling statistics, we
  can assess how close a sample statistic likely is to the parameter
  it's trying to estimate.

\end{frame}

\section{Sampling Distribution of the Sample Mean}

\subsection{Framework}

\begin{frame}
  \frametitle{Where does this fit in our roadmap?}
  
  We suppose that $X_1, \dots, X_n$ is a sample of size $n$ coming
  from some continuous distribution. We are interested in ascertaining
  the distribution of:

  $$\bar{x} = \frac{X_1 + \dots + X_n}{n}$$

  \vspace{3mm}

  For the purpose of estimating $\mu$, the population mean, with
  $\bar{x}$. We want to know whether $\bar{x}$ is even a good
  estimator of $\mu$, and if so understand its standard deviation (to
  know how far it is likely to be from $\mu$).

\end{frame}

\begin{frame}
  \frametitle{Notation}
  
  The population distribution that generated $X_1, \dots, X_n$ has a
  mean $\mu$ and a standard deviation $\sigma$. These may or may not
  be known when sampling.

  \vspace{3mm}

  The sampling distribution of $\bar{x}$ also has parameters that
  describe its center and dispersion. These are $\mu_{\bar{x}}$ and
  $\sigma_{\bar{x}}$.

  \vspace{3mm}

  We will have quantified the sampling distribution of $\bar{x}$ if we
  can determine the following:

  \begin{itemize}

    \item How do we determine the shape of the sampling distribution
      from the shape of the population distribution?

    \item How do we determine $\mu_{\bar{x}}$ from $\mu$?

    \item How do we determine $\sigma_{\bar{x}}$ from $\sigma$?
      
  \end{itemize}

\end{frame}

\subsection{Sampling Distribution for Normal Populations}

\begin{frame}
  
  Start with the simplest case: suppose that $X_1, \dots, X_n$ are
  samples from a population with a normal distribution. The normal
  distribution has the following useful property:

  \vspace{2mm}

  Let $X_1, \dots, X_n$ be samples from the distribution $N(\mu,
  \sigma)$. Then the sampling distribution of $\bar{x}$ is:

  $$N\left(\mu, \frac{\sigma}{\sqrt{n}}\right)$$

  I.e.:
  
  \begin{itemize}

    \item If the population distribution is normal, then so is the
      sampling distribution.

    \item The mean of the sampling distribution is the same as the
      mean of the population distribution.

    \item The sampling distribution's standard deviation is fairly
      simple: $\sigma_{\bar{x}} = \frac{\sigma}{\sqrt{n}}$
      
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose a golfer hits drives with a length distribution of $N(305,
  15)$. This distribution was determined by recording the golfer's
  drives for several years.

  \vspace{3mm}

  What is the probability that the golfer hits an individual drive of
  312 yards or more?

  \vspace{11mm}

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose a golfer hits drives with a length distribution of $N(305,
  15)$. This distribution was determined by recording the golfer's
  drives for several years.

  \vspace{3mm}

  What is the probability that the golfer hits an individual drive of
  312 yards or more?

  $$\mbox{normalcdf}(312, 1E99, 305, 15) \approx 0.32$$

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose this same golfer hits nine drives in a row. What is the
  probability that the \textbf{average} of these nine drives is 312
  yards or more?

  \vspace{30mm}

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose this same golfer hits nine drives in a row. What is the
  probability that the \textbf{average} of these nine drives is 312
  yards or more?

  Let $\bar{x}$ be the average of these nine drives. We know that the
  distribution of $\bar{x}$ is $N(305, \frac{15}{\sqrt{9}}) = N(305,
  5)$.

  So the probability that the average of the nine drives is more than
  312 yard is:

  $$\mbox{normalcdf}(312, 1E99, 305, 05) \approx 0.08$$

\end{frame}

\subsection{Central Limit Theorem}

\begin{frame}
  \frametitle{What if the Population Distribution Isn't Normal?}

  We have seen that the normal distribution is a type of continuous
  distribution. As a result, we will often have continuous populations
  that aren't normally distributed (e.g. any skewed population
  distribution).

  \vspace{3mm}

  In such a case, what we've seen so far does not help us, since we
  required the population distribution to be normal. Is there a way to
  determine the shape and parameters of $\bar{x}$ without requiring
  normality? 

\end{frame}

\begin{frame}
  \frametitle{Statement of the Central Limit Theorem}

  Let $X_1, \dots, X_n$ be a sample of random variables from an
  arbitrary continuous population distribution.

  \vspace{3mm}

  \textbf{If} $n \geq 30$ (i.e. if we have at least 30 samples from the
  population), then:

  \begin{itemize}

    \item The distribution of $\bar{x}$ is \textbf{normal}, even
      though the distribution of $X_1, \dots, X_n$ was not.

    \item The mean of the sampling distribution of $\bar{x}$ is still
      equivalent to the population distribution mean.

    \item If we know the standard deviation of the population
      distribution, $\sigma$, then we still have
      $\sigma_{\bar{x}} = \frac{\sigma}{\sqrt{n}}$

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose the scores on a standardized test taken by high school
  students have a distribution which is negatively skewed with a mean
  of 62 points and a standard deviation of 14.7 points.

  \vspace{3mm}

  If 50 students in a class take this test, what is the probability
  that their \emph{average} score is 68 or higher?

  \vspace{30mm}

\end{frame}

\begin{frame}
  \frametitle{Example}
  
  Suppose the scores on a standardized test taken by high school
  students have a distribution which is negatively skewed with a mean
  of 62 points and a standard deviation of 14.7 points.

  \vspace{3mm}

  If 50 students in a class take this test, what is the probability
  that their \emph{average} score is 68 or higher?

  \vspace{3mm}

  Since the population distribution is skewed (and thus not normal),
  we can only continue if we can use the central limit theorem. Since
  $50 \geq 30$, we can.

  $$\mbox{normalcdf} \left( 68, 1E99, 62, \frac{14.7}{\sqrt{50}} \right)
  \approx 0.00195$$

\end{frame}

\subsection{When $\sigma$ is Unknown}

\begin{frame}
  
  So far, we have assumed that $\sigma$ (the standard deviation of the
  population distribution, be it normal or not) is known.

  \vspace{3mm}

  This is not very realistic, especially in the context of trying to
  estimate $\mu$ with $\bar{x}$. If we know $\sigma$, wouldn't we also
  know $\mu$?

  \vspace{3mm}

  If $\sigma$ is unknown, we can use the sample standard deviation as
  a substitute. To make it clear when we're doing this, we call the
  resulting sampling distribution standard deviation the
  \textbf{standard error}:

  $$\mbox{Standard Error}(\bar{x}) = \frac{s}{\sqrt{n}}$$

\end{frame}

\section{Sampling Distribution of the Sample Proportion}

\subsection{Framework}

\begin{frame}
  
  In the case of a binomial distribution, our data is in the form of a
  discrete random variable $X$, the number of successes out of $n$
  trials.

  \vspace{3mm}

  We know that the population has a parameter $p$, called the
  \emph{population proportion}, which corresponds to the probability
  of ``success'' in an individual trial (e.g. the true probability of
  a coin coming up heads).

  \vspace{3mm}

  We can estimate $p$ with $\hat{p} = \frac{X}{n}$. Notice that
  $\hat{p}$ is an average, and has a sampling distribution.

\end{frame}

\begin{frame}
  
  Finding the exact sampling distribution of $\hat{p}$ is difficult,
  so we rely on the central limit theorem. Thus we require that $n
  \geq 30$ to proceed.

  \vspace{3mm}

  Suppose that X is a sample from $\mbox{Binomial}(n, p)$ where $n
  \geq 30$. Then the sampling distribution of $\hat{p} = \frac{X}{n}$
  is:

  $$N \left ( p, \sqrt{\frac{p(1-p)}{n}} \right)$$

  \vspace{3mm}

  From here, we can work with the distribution of $\hat{p}$ just like
  any other normal distribution.

\end{frame}

\subsection{Example}

\begin{frame}
  
  Suppose that 40\% of all US voters are in favor of candidate A in an
  election. If a pollster takes a random sample of 2400 voters, what
  is the probability that at least 42\% of those surveyed will support
  candidate A?

\end{frame}

\begin{frame}

  We need to find the sampling distribution of $\hat{p}$, the
  percentage of voters in the pollster's survey who support candidate
  A. Then we can calculate the probability that is it over 42\%.

  This sampling distribution is:
  
  $$N \left( 0.4, \sqrt{\frac{0.4 \cdot 0.6}{2400}} \right)$$

  So:

  $$P(\hat{p} \geq 0.42) = \mbox{normalcdf} \left( 0.42, 1E99, 0.4,
  \sqrt{\frac{0.4 \cdot 0.6}{2400}} \right) \approx 0.02275$$

\end{frame}

\subsection{When $p$ is Unknown}

\begin{frame}
  
  As in the continuous case, it is often unrealistic to assume we
  known $p$, especially since $p$ is often what we're trying to
  estimate.

  \vspace{3mm}

  Provided that the central limit theorem holds, we can substitute the
  sampling distribution standard deviation $\sqrt{\frac{p(1-p)}{n}}$
  with the corresponding \textbf{standard error}:

  $$\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}$$

\end{frame}



\end{document}
